//
//  WeatherTest.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/5/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import XCTest
@testable import WeatherApp

class WeatherTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let testJson = "{\"coord\":{\"lon\":139.01,\"lat\":35.02},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"base\":\"stations\",\"main\":{\"temp\":285.514,\"pressure\":1013.75,\"humidity\":100,\"temp_min\":285.514,\"temp_max\":285.514,\"sea_level\":1023.22,\"grnd_level\":1013.75},\"wind\":{\"speed\":5.52,\"deg\":311},\"clouds\":{\"all\":0},\"dt\":1485792967,\"sys\":{\"message\":0.0025,\"country\":\"JP\",\"sunrise\":1485726240,\"sunset\":1485763863},\"id\":1907296,\"name\":\"Tawarano\",\"cod\":200}\r\n"
        let data = testJson.data(using: .utf8)
        
        let json = try? JSONSerialization.jsonObject(with: data!) as! [String: Any]
        let weather = Weather(json: json!)!
        
        XCTAssertEqual(weather.coord.lon, 139.01)
        XCTAssertEqual(weather.coord.lat, 35.02)
        XCTAssertEqual(weather.date, 1485792967)
    }
    
}
