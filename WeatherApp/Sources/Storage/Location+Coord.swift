//
//  Location+Coord.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/5/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import CoreData
import UIKit

extension Location {
    convenience init(coord: Coord, name: String?) {
        self.init(context: Storage.shared.managedObjectContext)
        self.longitude = coord.lon
        self.latitiude = coord.lat
        self.name = name
        self.time = Date() as NSDate
    }
}
