//
//  WeatherService.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/4/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import UIKit

let apiKey = "f997de1ef54e96403258fbcb85b805e2"
let serviceUrl = "http://api.openweathermap.org/data/2.5/weather"

struct ServiceError: Error {
    let _domain: String
    let _code: Int
}

typealias WeatherCallback = (Result<Weather>) -> Void

class WeatherService {

    static let shared = WeatherService()

    lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        return session
    }()

    func weather(forLocation location: Location, completion: WeatherCallback?) {
        let request = weatherRequest(forLocation: location)

        let task = session.dataTask(with: request, completionHandler: { data, _, error in

            if let error = error {
                completion?(Result.failure(error))
            } else {
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]
                        if let weather = Weather(json: json!) {
                            completion?(Result<Weather>.success(weather))
                        } else {
                            completion?(Result.failure(ServiceError(_domain: "Invalid json", _code: 0)))
                        }
                    } catch let error {
                        completion?(Result.failure(error))
                    }
                } else {
                    completion?(Result.failure(ServiceError(_domain: "No Data returned", _code: 0)))
                }
            }
        })
        task.resume()
    }

    func weatherRequest(forLocation location: Location) -> URLRequest {
        let parametersString = "?lat=\(location.latitiude)&lon=\(location.longitude)&appid=\(apiKey)"
        let requestUrl = serviceUrl + parametersString

        //at this point better to crash in case of invalid url
        let url = URL(string: requestUrl)!
        return URLRequest(url: url)
    }

}
