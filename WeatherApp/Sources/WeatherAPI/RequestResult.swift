//
//  RequestResult.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/5/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}
