//
//  Forecast.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/5/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import Foundation

struct Wind {
    let speed: Float
    let deg: Float

    init(json: [String: Any]) {
        speed = (json["speed"] as? Float) ?? 0
        deg = (json["deg"] as? Float) ?? 0
    }

    init(lat: Float = 0, lon: Float = 0) {
        self.speed = lat
        self.deg = lon
    }
}

struct Weather {
    let coord: Coord

    let id: Int
    let main: String
    let weatherDescription: String
    let icon: String

    let temp: Float
    let pressure: Float
    let humidity: Float
    let tempMin: Float
    let tempMax: Float

    let wind: Wind

    let clouds: Int

    let date: UInt64

    let rain: Float?

    let snow: Float?

    let cityName: String

    init?(json: [String: Any]) {
        if let coordJson = json["coord"] as? [String: Any] {
            coord = Coord(json: coordJson)
        } else {
            coord = Coord()
        }

        if let windJson = json["wind"] as? [String: Any] {
            wind = Wind(json: windJson)
        } else {
            wind = Wind()
        }

        guard let main = json["main"] as? [String: Any] else { return nil }

        guard let temp = main["temp"] as? Float,
            let pressure = main["pressure"] as? Float,
            let humidity = main["humidity"] as? Float,
            let temp_min = main["temp_min"] as? Float,
            let temp_max = main["temp_max"] as? Float else { return nil }
        self.temp = temp
        self.pressure = pressure
        self.humidity = humidity
        self.tempMin = temp_min
        self.tempMax = temp_max

        guard let weather = (json["weather"] as? [Any])?.first as? [String: Any] else { return nil }

        guard let id = weather["id"] as? Int,
            let mainWeather = weather["main"] as? String,
            let weatherDescription = weather["description"] as? String,
            let icon = weather["icon"] as? String else { return nil }
        self.id = id
        self.main = mainWeather
        self.weatherDescription = weatherDescription
        self.icon = icon

        if let cloudsJson = json["clouds"] as? [String: Any] {
            clouds = cloudsJson["all"] as? Int ?? 0
        } else {
            clouds = 0
        }

        if let rainJson = json["rain"] as? [String: Any] {
            rain = rainJson["3h"] as? Float ?? 0
        } else {
            rain = 0
        }

        if let snowJson = json["snow"] as? [String: Any] {
            snow = snowJson["3h"] as? Float ?? 0
        } else {
            snow = 0
        }

        cityName = (json["name"] as? String) ?? "No city name"
        date = (json["dt"] as? UInt64) ?? 0
    }

}
