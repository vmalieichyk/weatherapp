//
//  Coord.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/5/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import Foundation

struct Coord {
    let lat: Float
    let lon: Float

    init(json: [String: Any]) {
        lat = (json["lat"] as? Float) ?? 0
        lon = (json["lon"] as? Float) ?? 0
    }

    init(lat: Float = 0, lon: Float = 0) {
        self.lat = lat
        self.lon = lon
    }

}
