//
//  ViewController.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/3/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import CoreData
import UIKit

class LocationsVC: UICollectionViewController, LocationSelectorDelegate, NSFetchedResultsControllerDelegate {

    var selectedItems = Set<Location>()

    @IBOutlet weak var deleteButton: UIBarButtonItem!
    @IBOutlet weak var endEditingButton: UIBarButtonItem!
    @IBOutlet weak var addLocationButton: UIBarButtonItem!
    lazy var longPressGestureRecognizer: UILongPressGestureRecognizer = {
        let longPressGestureRecgnizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongTap))
        return longPressGestureRecgnizer
    }()

    lazy var fetchedResultsController: NSFetchedResultsController<Location> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Location")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "time", ascending: true)]

        let context = Storage.shared.managedObjectContext
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)

        fetchedResultsController.delegate = self

        return fetchedResultsController as! NSFetchedResultsController<Location>
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setRightBarButtonItems([addLocationButton], animated: true)

        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.userInfo)")
        }

        self.view.addGestureRecognizer(longPressGestureRecognizer)
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if isEditing && identifier == "LocationWeather" {
           return false
        }
        return true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LocationSelector" {
            if let viewController = segue.destination as? LocationSelectorVC {
                viewController.delegate = self
            }
        } else if segue.identifier == "LocationWeather" {
            if let viewController = segue.destination as? LocationWeatherVC {
                let indexPath = self.collectionView?.indexPathsForSelectedItems?.first
                viewController.location = fetchedResultsController.object(at: indexPath!)
            }
        }
    }

    // MARK: item deletion

    @objc fileprivate func handleLongTap(recognizer: UILongPressGestureRecognizer!) {
        enterEditingMode()
        let location = recognizer.location(in: collectionView)
        if let indexPath = collectionView?.indexPathForItem(at: location) {
            selectedItems.insert(fetchedResultsController.object(at: indexPath))
            collectionView?.reloadItems(at: [indexPath])
        }

    }

    fileprivate func enterEditingMode() {
        isEditing = true
        addEditingBarButtons()
    }

    fileprivate func endEditingMode() {
        selectedItems.removeAll()
        isEditing = false
        self.navigationItem.setRightBarButtonItems([addLocationButton], animated: true)
    }

    fileprivate func addEditingBarButtons() {
        self.navigationItem.setRightBarButtonItems([deleteButton, endEditingButton], animated: true)
    }

    @IBAction func doneButtonAction(_ sender: Any) {
        endEditingMode()
    }

    @IBAction func deleteButtonAction(_ sender: Any) {
        let context = Storage.shared.managedObjectContext
        selectedItems.forEach { item in
             context.delete(item)
        }
        endEditingMode()
        try? context.save()
        collectionView?.reloadData()
    }

    // MARK: data source

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections.count
        }

        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }

        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationCell", for: indexPath) as! LocationCell

        configureCell(cell, atIndexPath:indexPath)

        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isEditing {
            let item = fetchedResultsController.object(at: indexPath)
            if selectedItems.contains(item) {
                selectedItems.remove(item)
            } else {
                selectedItems.insert(item)
            }
            collectionView.reloadItems(at: [indexPath])
        }
    }

    fileprivate func configureCell(_ cell: LocationCell, atIndexPath indexPath: IndexPath) {
        let item = fetchedResultsController.object(at: indexPath)

        cell.nameLabel.text = item.name
        cell.latitudeLabel.text = "lat: \(item.latitiude)"
        cell.longitudeLabel.text = "lon: \(item.longitude)"

        if selectedItems.contains(item) {
            cell.backgroundColor = UIColor.darkGray
        } else {
            cell.backgroundColor = UIColor.lightGray
        }
    }

    // MARK: LocationSelectorDelegate

    func didSelectLocation(coord: Coord, name: String?) {
        let location = Location(coord: coord, name: name)
        try? location.managedObjectContext?.save()
    }

    // MARK: FetchedResultsControllerDelegate

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {

    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {

    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.collectionView?.insertItems(at: [indexPath])
            }
            break
        case .delete:
            if let indexPath = newIndexPath {
                self.collectionView?.deleteItems(at: [indexPath])
            }
            print("delete item")
        default:
            print("")
        }
    }

}
