//
//  LocationCell.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/5/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import UIKit

class LocationCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
}
