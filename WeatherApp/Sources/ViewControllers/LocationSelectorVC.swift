//
//  LocationSelectorVC.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/4/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import MapKit
import UIKit

protocol LocationSelectorDelegate {
    func didSelectLocation(coord: Coord, name: String?)
}

class LocationSelectorVC: UIViewController {
    var delegate: LocationSelectorDelegate?
    var singleTapRecognizer: UITapGestureRecognizer!

    var selectedLocationPlacemark: CLPlacemark?
    var selectedLat: Float?
    var selectedLon: Float?
    var address: String?
    var selectedAddressDescription: String?

    fileprivate var pinAnnotation: MKPointAnnotation? {
        didSet {
            self.doneButton.isEnabled = pinAnnotation != nil
        }
    }

    @IBOutlet private weak var mapView: MKMapView!
    var doneButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addDoneBarButton()
        self.doneButton.isEnabled = false

        addMapTapGesture()
    }

    // MARK: 

    func removeAllAnnotations() {
        mapView.removeAnnotations(mapView.annotations)
        pinAnnotation = nil
    }

    func didSelectDone() {
        if let pin = pinAnnotation {
            selectedLat = Float(pin.coordinate.latitude)
            selectedLon = Float(pin.coordinate.longitude)
        }
        self.delegate?.didSelectLocation(coord: Coord(lat: selectedLat!, lon:selectedLon!), name: self.address)

        self.navigationController?.popViewController(animated: true)
    }

    // MARK: private
    fileprivate func addMapTapGesture() {
        singleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        singleTapRecognizer.numberOfTapsRequired = 1
        mapView.addGestureRecognizer(singleTapRecognizer)
    }

    fileprivate func addDoneBarButton() {
        doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(didSelectDone))
        self.navigationItem.setRightBarButton(doneButton, animated: false)
    }

    @objc fileprivate func handleTap(recognizer: UITapGestureRecognizer!) {
        let point = recognizer.location(in: mapView)

        let tapPoint = mapView.convert(point, toCoordinateFrom: self.mapView)

        handleLocation(withCoordinate: tapPoint)
        searchAddressForLocation(location: CLLocation(latitude: tapPoint.latitude, longitude: tapPoint.longitude))
    }

    fileprivate func handleLocation(withCoordinate coordinate: CLLocationCoordinate2D) {
        removeAllAnnotations()
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        pinAnnotation = annotation
        mapView.addAnnotation(annotation)
    }

    fileprivate func searchAddressForLocation(location: CLLocation) {
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {[weak self] (placemarks, error) -> Void in
            if let error = error {
                print(error.localizedDescription)
            } else if let placemarks = placemarks, placemarks.isEmpty {
                print("no places detected here")
            } else {
                if let addressDictionary = placemarks?.first?.addressDictionary {
                    self?.address = self?.addressStringFromAddressDictionary(addressDictionary)
                    print(self?.address ?? "no address")
                }
            }
        })
    }

    fileprivate func addressStringFromAddressDictionary(_ addressDictionary: [AnyHashable: Any]) -> String {
        var address = String()

        if let street = addressDictionary["Street"] as? String {
            address.append(street)
        }
        if let state = addressDictionary["State"] as? String {
            if !address.characters.isEmpty { address += ", " }
            address.append(state)
        }
        if let city = addressDictionary["City"] as? String {
            if !address.characters.isEmpty { address += ", " }
            address.append(city)
        }
        if let country = addressDictionary["Country"] as? String {
            if !address.characters.isEmpty { address += ", " }
            address.append(country)
        }
        if let postalCode = addressDictionary["ZIP"] as? String {
            if !address.characters.isEmpty { address += ", " }
            address.append(postalCode)
        }
        return address
    }

}
