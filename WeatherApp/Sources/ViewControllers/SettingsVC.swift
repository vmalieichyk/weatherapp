//
//  SettingsVC.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/4/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import UIKit

class SettingsVC: UITableViewController {

    @IBOutlet weak var unitsSwitch: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()

        let settings = UserDefaults.standard
        let settingsValue = settings.value(forKey: "metricUnits") as? NSNumber
        self.unitsSwitch.isOn = settingsValue?.boolValue ?? true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func metricUnitsSwitch(_ sender: Any) {
        let settings = UserDefaults.standard
        settings.set(self.unitsSwitch.isOn, forKey: "metricUnits")
    }

}
