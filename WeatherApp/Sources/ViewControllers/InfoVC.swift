//
//  InfoVC.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/6/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import UIKit

let infoPageAddress = "http://openweathermap.org/about"

class InfoVC: UIViewController {
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string:infoPageAddress)!

        webView.loadRequest(URLRequest(url: url))
    }
}
