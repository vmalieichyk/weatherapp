//
//  LocationWeatherVC.swift
//  WeatherApp
//
//  Created by Viktor Malieichyk on 6/4/17.
//  Copyright © 2017 viktor.malieichyk. All rights reserved.
//

import UIKit

class LocationWeatherVC: UIViewController {
    @IBOutlet weak var detailedWeatherTextView: UITextView!

    var location: Location!

    override func viewDidLoad() {
        super.viewDidLoad()

        WeatherService.shared.weather(forLocation: location, completion: { result in
            print(result)
            DispatchQueue.main.async {
                switch result {
                case .success(let result):
                    self.detailedWeatherTextView.text = String(describing:result)
                case .failure(let error):
                    self.detailedWeatherTextView.text = String(describing:error)
                }
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

}
